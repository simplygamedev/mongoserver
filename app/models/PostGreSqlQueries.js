'use strict';

const INSERT = {
    ONE: {
        USER:'INSERT INTO USERS(username, password) VALUES($1, $2)',
    },
    MULTIPLE: {

    }
};

const READ = {
    ONE:{
        USER: 'SELECT * FROM USERS WHERE username = $1'
    },
    MULTIPLE:{
        USER:'SELECT * FROM USERS'
    }
};

const UPDATE = {

};

const DELETE = {

};

module.exports = {
    insertOneUser: INSERT.ONE.USER,
    queryOneUser: READ.ONE.USER,
    queryMultipleUsers:READ.MULTIPLE.USER
};