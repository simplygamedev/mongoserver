'use strict';

/* Test is the Collection we wish to add to,
   while testSchema is the template Record we wish to add */
module.exports = {
    Test: require('./MongoDBSchemas/testSchema'),
};