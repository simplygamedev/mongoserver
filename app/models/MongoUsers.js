'use strict';

const mongoose = require('mongoose'),
bcrypt = require('bcrypt-nodejs');

let userSchema = new mongoose.Schema({
    username:{
        type: String,
        required: true,
        unique: true
    },
    password:{
        type: String,
        required: true
    },
    isAdmin:{
        type:Boolean,
        default:false
    }
});

/* This is called a pre-hook, before the user information is saved in the database
   this function will be called, we'll get the plain text password, hash it and store it. */
   userSchema.pre('save', async function(next){

        /* 'this' refers to the current document about to be saved */
        const user = this;

        /* Hash the password with a salt round of 10, the higher the rounds the more secure, 
        but the slower your application becomes. */

        /* Replace the plain text password with the hash and then store it */
        let salt = bcrypt.genSaltSync(10);
        const hash = await bcrypt.hashSync(user.password, salt);
        user.password = hash;

        /* Indicates we're done and moves on to the next middleware */
        next();
  });
  
  /* We'll use this later on to make sure that the user trying to log in has the correct credentials */
  userSchema.methods.isValidPassword = async function(password){
    const user = this;
    /* Hashes the password sent by the user for login and checks if the hashed password stored in the 
       database matches the one sent. Returns true if it does else false. */
    const compare = await bcrypt.compareSync(password, user.password);
    return compare;
  }

exports.RequestIsUserSchemaFormat = function(request){

    /* Checks to ensure that the request object's properties are the correct ones for the testSchema */
    let properties = Object.keys(request);
    for(let property in properties){
        switch(properties[property]){
            case 'name':
            case 'password':
            case 'isAdmin':
            case '_id':
                break;
            default:
                return false;
        }
    }

    return true;
}

exports.userSchema = mongoose.model('User', userSchema);