'use strict'

const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);

const schemaName = 'Test';

const Schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
});

const CheckFormat = function (request) {
    /* Checks to ensure that the request object's 
       properties are the correct ones for the Schema */
    let match = ['name', '_id'];
    let properties = Object.keys(request);
    for (let property in properties) {
        if (match.indexOf(properties[property]) <= -1) {
            return false;
        }
    }

    return true;
};

module.exports = {
    model: mongoose.model(schemaName, Schema),
    checkFormat: CheckFormat
};
