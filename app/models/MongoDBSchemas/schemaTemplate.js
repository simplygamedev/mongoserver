'use strict'

const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);

/* https://medium.freecodecamp.org/introduction-to-mongoose-for-mongodb-d2a7aa593c57
   This Schemas are mongoose representation of MongoDB Records */

// You should only need to change schemaName, add to Schema variable and CheckFormat

const schemaName = 'Change to Your Schemas Name';

const Schema = new mongoose.Schema({
    //In here you should put all the fields/columns your schema is supposed to contain
});

const CheckFormat = function (request) {
    /* Return true or false here depending on whether request object's 
       properties are the correct ones for the Schema */

    return false;
};

module.exports = {
    model: mongoose.model(schemaName, Schema),
    checkFormat: CheckFormat
};