'use strict';

const config = require('../../config/env/' + ((process.env.NODE_ENV!==undefined) ? process.env.NODE_ENV : 'test'));
const passport = require('../middleware/' + config.passportjs.authentication),
bodyParser = require('body-parser'),
json = bodyParser.json();

module.exports = function(app) {

    app.post('/signup', json, passport.SignUpUser);
    app.post('/login', json, passport.Login);

    app.get('/logout', (req, res) => {
        delete req.user;
        delete req.session;
        req.logout();
        return res.sendStatus(204);
    });
};