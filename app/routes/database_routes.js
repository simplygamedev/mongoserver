'use strict';

const env = process.env.NODE_ENV !== undefined
    ? process.env.NODE_ENV
    : 'test';

const config = require(`../../config/env/${env}`);

const database = require('../database/database'),
    passport = require('../middleware/' + config.passportjs.authentication),
    bodyParser = require('body-parser');

const urlencoded = bodyParser.urlencoded({ extended: true, type: 'application/x-www-form-urlencoded' }),
    json = bodyParser.json();

module.exports = function (app) {
    app.post('/api/insertRecord/:collection', urlencoded, passport.isAdmin, database.insertRecord);
    app.post('/api/insertMultipleRecords/:collection', json, passport.isAdmin, database.insertMultipleRecords);

    app.delete('/api/deleteAllRecords/:collection', passport.isAdmin, database.deleteAllRecords);
    app.delete('/api/deleteOneRecord/:collection', urlencoded, passport.isAdmin, database.deleteOneRecord);

    app.get('/api/queryOneRecord/:collection', urlencoded, passport.isAuthenticated, database.queryOneRecord);
    app.get('/api/queryAllRecords/:collection', urlencoded, passport.isAuthenticated, database.queryAllRecords);

    app.put('/api/updateOneRecord/:collection', json, passport.isAdmin, database.updateOneRecord);
};