'use strict';

const passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    UserModel = require('../models/MongoUsers').userSchema;

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    UserModel.findById(id, (err, user) => {
        done(err, user);
    });
});

/* Create a passport middleware to handle user registration */
passport.use('signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
},
    async (username, password, done) => {
        try {
            /* Save the information provided by the user to the the database */
            const user = await UserModel.create({ username: username, password: password });
            return done(null, user); /* Send the user information to the next middleware */
        }
        catch (error) {
            done(error);
        }
    }
));

/* Create a passport middleware to handle User login 
   Requests should be sent in the format:
    { 
        "email": "username",
        "password": "password"
    } 
*/
passport.use('login', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
},
    async (username, password, done) => {
        try {
            /* Find the user associated with the email provided by the user */
            const user = await UserModel.findOne({ username: username });

            if (!user)
                return done(null, false, { message: 'User not found' }); /* If the user isn't found in the database, return a message */

            /* Validate password and make sure it matches with the corresponding hash stored in the database
               If the passwords match, it returns a value of true. */
            const validate = await user.isValidPassword(password);

            if (!validate) {
                return done(null, false, { message: 'Invalid Password' });
            }

            /* Send the user information to the next middleware */
            return done(null, user, { message: 'Logged in Successfully' });
        }
        catch (error) {
            return done(error);
        }
    }
));

function Login(req, res, next) {
    passport.authenticate('login', (err, user) => {
        try {
            if (err || !user)
                return res.status(400).json('Invalid User Credentials or Error Logging In.');

            req.login(user, (error) => {

                if (error) {
                    return res.status(400).json('Error Logging User In. Please Try Again.');
                }

                /* We don't want to store the sensitive information such as the
                user password in the token so we pick only the username and id */
                const body = {
                    _id: user._id,
                    username: user.username
                };

                return res.status(200).json({
                    message: 'Login is Successful',
                    user: body
                });

            });
        }
        catch (error) {
            return res.status(400).json(error);
        }

    })(req, res, next);
};

function isAdmin(req, res, next) {
    if (!req.user || (req.user && req.user.isAdmin === false)) {
        return res.status(403).json('User is not authorised to access this route.');
    }
    next();
};

function SignUpUser(req, res, next) {
    passport.authenticate('signup', async (err, user) => {
        if (err || !user) {
            return res.status(400).json(err);
        }

        req.login(user, error => {
            if (error) {
                return res.status(400).json('Error Logging User In. Please Try Again.');
            }

            /* We don't want to store the sensitive information such as the
            user password in the token so we pick only the username and id */
            const body = {
                _id: user._id,
                username: user.username
            };

            return res.status(200).json({
                message: 'Signup Was Successful',
                user: body
            });
        });

    })(req, res, next);
}

function isAuthenticated(req, res, next) {
    req.user
        ? next()
        : res.status(403).json('Not Logged in as a User');
}

module.exports = {
    Login: Login,
    SignUpUser: SignUpUser,
    isAdmin: isAdmin,
    isAuthenticated: isAuthenticated,
    object: passport
};