'use strict';

let Schemas = require('../models/Schemas'),
    _ = require('underscore');

/* https://stackoverflow.com/questions/20089582/how-to-get-a-url-parameter-in-express 
   https://medium.com/@fullsour/when-should-you-use-path-variable-and-query-parameter-a346790e8a6d */
function GetSchemaInfo(req) {
    /* This Method Checks To Ensure The Collection 
       Specified in the URL's Path Variable Exists */
    return [
        Schemas[req.params.collection].model,
        Schemas[req.params.collection].checkFormat
    ];
}

/* Start Create Section */
exports.insertRecord = async function (req) {
    const [schema, checkSentRequest] = GetSchemaInfo(req);

    /* Start Validate Request Section
       Ensure Value Received is appropriate  */
    if (!checkSentRequest(req.body))
        throw { message: 'Request is not in the appropriate Schema Format' };

    /* Create the collection and 
       attempt to save it to mongo database */
    let collection = new schema(req.body);

    let result = await collection.save()
        .then(() => {
            return req.body;
        });

    return result;
}

exports.insertMultipleRecords = async function (req) {
    /* Data is to be sent as a JSON 
       E.g. application/json */
    let data = req.body;
    let schema, checkSentRequest;
    [schema, checkSentRequest] = GetSchemaInfo(req);

    let check = data.map(entry => {
        return checkSentRequest(entry);
    });

    if (check.includes(false)) {
        throw { message: 'Request sent contains data not in the appropriate Schema Format' };
    }

    let result = await schema.collection.insertMany(data)
        .then(() => {
            return data;
        });

    return result;
};
/* End Create Section */


/* Start Delete Section*/
exports.deleteAllRecords = async function (req) {
    const [schema, checkSentRequest] = GetSchemaInfo(req);

    await schema.deleteMany({});
}

exports.deleteOneRecord = async function (req) {
    const [schema, checkSentRequest] = GetSchemaInfo(req);

    /* Start Validate Request Section
       Ensure Value Received is appropriate  */
    if (!checkSentRequest(req.body))
        throw { message: 'Request is not in the appropriate Schema Format' };

    let result = await schema.findOne(req.query)
        .then((document) => {
            return document;
        });

    if (!result) {
        return false;
    }

    await new Promise((resolve, reject) => {
        schema.findOneAndDelete(req.body, err => {
            if (err) { reject(err); }
            resolve();
        })
    });

    return true;
};
/* End Delete Section*/


/* Refer to this link for MongoDB CRUD Operations 
   https://coursework.vschool.io/mongoose-crud/ 
   https://www.tutorialkart.com/nodejs/mongoose/insert-multiple-documents-to-mongodb/*/

/* Start Read Section */
exports.queryOneRecord = async function (req) {
    const [schema, checkSentRequest] = GetSchemaInfo(req);

    /* Start Validate Request Section
       Ensure Value Received is appropriate  */
    if (!checkSentRequest(req.body)){
        throw { message: 'Request is not in the appropriate Schema Format' };}

    const result = await schema.findOne(req.body)
        .then((document) => {
            return document ? document : 'No Records were found.';
        });

    return result;
};

exports.queryAllRecords = async function (req) {
    const [schema] = GetSchemaInfo(req);

    let result = await schema.find({})
        .then((documents) => {
            return documents ? documents : 'No Records were found.';
        });

    return result;
};
/* End Read Section */


/* Start Update Section 
   Format for json string input
   {  query: <Query Object Here>, 
      update: <Object Containing The Desired Updates Here> }  */
exports.updateOneRecord = async function (req) {
    const data = req.body;
    const [schema, checkSentRequest] = GetSchemaInfo(req);

    if (!checkSentRequest(data.query)) {
        throw { message: 'Request query is not in the appropriate Schema Format' };
    }

    let modified = _.extend({}, data.query, data.update);

    let result = await schema.collection.updateOne(data.query, { $set: modified })
        .then((document) => {
            return document;
        });

    return result;
};
/* End Update Section */