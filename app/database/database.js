'use strict';

let env = require('../../config/env/' + ((process.env.NODE_ENV !== undefined) ? process.env.NODE_ENV : 'test'));
let Database = require('./' + env.db);

exports.insertRecord = async function (req, res) {
    await Database.insertRecord(req)
        .then((result) => { res.status(200).json(result); })
        .catch((err) => { res.status(400).json(err); });
};

exports.insertMultipleRecords = async function (req, res) {
    await Database.insertMultipleRecords(req)
        .then((result) => {
            res.status(200).json(result);
        })
        .catch((err) => {
            res.status(400).json(err);
        });
};

exports.deleteAllRecords = async function (req, res) {
    await Database.deleteAllRecords(req)
        .then(() => {
            res.sendStatus(204);
        })
        .catch((err) => {
            res.status(400).json(err);
        });
};

exports.deleteOneRecord = async function (req, res) {
    await Database.deleteOneRecord(req)
        .then((result) => {
            result ? res.sendStatus(204) : res.status(400).json('The Content You Are Attempting To Delete Does Not Exist');
        })
        .catch((err) => { res.status(400).json(err); });
};

exports.queryOneRecord = async function (req, res) {
    await Database.queryOneRecord(req)
        .then((result) => {
            res.status(200).json(result);
        })
        .catch((err) => {
            res.status(400).json(err);
        });
};

exports.queryAllRecords = async function (req, res) {
    await Database.queryAllRecords(req)
        .then((result) => {
            res.status(200).json(result);
        })
        .catch((err) => {
            res.status(400).json(err);
        });
};

exports.updateOneRecord = async function (req, res) {
    await Database.updateOneRecord(req)
        .then((result) => {
            res.status(200).json(result);
        })
        .catch((err) => {
            res.status(400).json(err);
        });
};