'use strict';

const queries = require('../models/PostGreSqlQueries'),
      config = require('../../config/env/' + ((process.env.NODE_ENV !== undefined) ? process.env.NODE_ENV : 'test'));

const { Pool } = require('pg'),
      pool = new Pool(config.postgresql);

function checkCorrectnessOfURLPathVariable(req) {
      switch (req.params.collection) {
            case 'Users':
                  return true;
            default:
                  return undefined;
      }
}

/**
 * Possible suggestion to reduce duplicity of the code
 * Perhaps for the checkCorrectnessOfURLPathVariable() method,
 * We could revamp the entire script such that we create classes corresponding to the various database tables that we have, 
 * We make a abstract table class whereby all other classes will inherit from it, & this abstract table class should contain the checkCorrectnessOfURLPathVariable.
 * The Child table classes would then have their own respective insertOneItem method and insertMultipleItems etc methods.
 * https://ilikekillnerds.com/2015/06/abstract-classes-in-javascript/
 */

/* Start Create Section */
exports.insertRecord = async function (req) {
      if (!checkCorrectnessOfURLPathVariable(req)) {
            throw { message: 'Invalid URL Path Variable' };
      }

      const client = await pool.connect();

      try {
            return await client.query(queries.insertOneUser, [req.body.username, req.body.password]);
      }
      catch (e) {
            const err = {
                  message: 'Error Sending Insert Query'
            };

            if (e && e.code === '23505') {
                  err.message = 'Primary Key Already Exists.';
            }

            throw err;
      }
      finally {
            client.release();
      }
}

exports.insertMultipleRecords = async function (req) {
      /* Data is to be sent as a JSON 
         E.g. application/json */
      await client.connect();
};
/* End Create Section */


/* Start Delete Section*/
exports.deleteAllRecords = async function (req) {
      await client.connect();
}

exports.deleteOneRecord = async function (req) {
      await client.connect();
};
/* End Delete Section*/


/* Start Read Section */
exports.queryOneRecord = async function (req) {
      let result;

      if (!checkCorrectnessOfURLPathVariable(req)) {
            throw { 
                  message: 'Invalid URL Path Variable' 
            };
      }

      console.log(req.body);

      try {
            await client.connect();
            return await client.query(queries.queryOneUser, [req.body.username]).then((res) => { return res; });
      }
      catch (e) {
            throw e instanceof Error
                  ? e
                  : new Error(e);
      }
};

exports.queryAllRecords = async function (req) {
      await client.connect();
};
/* End Read Section */


/* Start Update Section 
   Format for json string input
   {  query: <Query Object Here>, 
      update: <Object Containing The Desired Updates Here> }  */
exports.updateOneRecord = async function (req) {
      await client.connect();
};
/* End Update Section */