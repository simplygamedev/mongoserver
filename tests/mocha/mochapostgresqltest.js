'use strict';

const assert = require('assert'),
axios = require('axios');

/* Require the dev-dependencies */
let chai = require('chai'),
    chaiHttp = require('chai-http'),
    serverobject = require('../../server'),
    server = require('../../server').app,
    _ = require('underscore');

let mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

chai.use(chaiHttp);

var agent = chai.request.agent(server);

describe('PostGreSql API Tests', function () {

    this.timeout(0);

    /* Ensures Server has started before starting any tests */
    this.beforeAll((done)=>{
        server.on('Server Started', ()=>{
            done();
        });
    });

    after(()=>{
        agent.close();
        serverobject.server.close();
        mongoose.connection.close();
    });

    describe('/POST test to route /api/insertRecord/:collection', function(){
        
        it('Should POST to `Test` Collection', (done)=>{
            done();
        });

        it('Should return a 400 error on non-existent Path variable provided', (done)=>{
            done();
        });

        it('Should return a 400 error message on attempted POST of object with wrong property to `Test` Collection', (done)=>{
            done();
        });

        it('Should return a 400 error message on attempted POST of object with existing entry in `Test` Collection', (done)=>{
            done();
        });
    });

    /*describe('/POST test to route /api/insertMultipleRecords/:collection', ()=>{

        it('Should POST to `TEST` collection and return 200 status message and array of Object', (done)=>{
        
        });

        it('Should return a 400 error on non-existent Path variable provided', (done)=>{

        });

        it('Should return a 400 error message on attempted POST of objects with any object containing a wrong property to `Test` Collection', (done)=>{

        });

        it('Should return a 400 error on attempted POST of any objects already existing within the database', (done)=>{
        
        });
    });

    describe('/DELETE test to route /api/deleteAllRecords/:collection', ()=>{

        it('Should DELETE all records in database', (done)=>{

        });

        it('Should return 204 response if attempting to delete from empty database', (done)=>{

        });
    });


    describe('/DELETE test to route /api/deleteOneRecord/:collection', ()=>{

        it('Should delete one record in database if record exist', (done)=>{
        
        });

        it('Should return 400 error on attempt to delete non-existent entry', (done)=>{

        });

        it('Should return 400 error if provided Non-existent Path variable', (done)=>{

        });

        it('Should return a 400 error message on attempted DELETE of object in `Test` Collection with a non-existent property of `Test`', (done)=>{

        });
    });


    describe('/GET test to route /api/queryOneRecord/:collection', ()=>{
    
        it('Should manage to query and return existing entries in database', (done)=>{

        });

        it('Should return 400 error message when provided non-existent Path variable', (done)=>{

        });

        it('Should return a 400 error message on attempted GET of object in `Test` Collection with a non-existent property of `Test`', (done)=>{

        });

        it('Should return 200 status message with result null on attempt to GET an entry that doesn`t exist in database', (done)=>{

        });
    });

    describe('/GET test to route /api/queryAllRecords/:collection', ()=>{

        it('Should manage to query and return all existing entries in database', (done)=>{

        });

        it('Should return 400 error message when provided non-existent Path variable', (done)=>{

        });
    });

    describe('/PUT test to route /api/updateOneRecord/:collection', ()=>{

        it('Should update property of existing objects', (done)=>{

        });

        it('Should return 400 error message when provided non-existent Path variable', (done)=>{

        });

        it('Should return 400 error if request contains non-existent property of `Test` collection in query property', (done)=>{

        });

        it('Should return 200 message with nModified == 0 on attempt to update an entry that does not exist in database', (done)=>{

        }); 
    });*/
});