'use strict';

const assert = require('assert'),
    config = require('../../../config/env/test');

/* https://codeburst.io/javascript-unit-testing-using-mocha-and-chai-1d97d9f18e71 
   https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai 
   https://gist.github.com/samwize/8877226 
   https://gist.github.com/stewartmccoy/641c4170e6e6d1519b0fe2ffac78bec2*/

/* Require the dev-dependencies */
let chai = require('chai'),
    chaiHttp = require('chai-http'),
    serverobject = require('../../../server'),
    server = serverobject.app,
    should = chai.should(),
    _ = require('underscore');

let mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

let Schema = require('../../../app/models/Schemas'),
    User = require('../../../app/models/MongoUsers'),
    bcrypt = require('bcrypt-nodejs');

chai.use(chaiHttp);

var agent = chai.request.agent(server);

let AdminUser = {
    username: 'Jonathan',
    password: 'password'
};

let notAdminUser = {
    username: 'Putin',
    password: 'pass'
};

async function addUser(isAdmin, user) {
    let entry = _.extend({}, user, { isAdmin: isAdmin });
    await User.userSchema.collection.insertOne(entry);
}

async function loginUser(user) {

    await new Promise((resolve, reject) => {
        agent
            .post('/login')
            .set({ 'content-type': 'application/json' })
            .send(_.extend({}, AdminUser))
            .end((err, res) => {
                if (err)
                    reject(err);
                res.should.have.status(200);
                resolve(res.body);
            });
    })
        .then((res) => {
            return res;
        })
        .catch((err) => {
            console.error('Login User Error:', err);
            return err;
        });
}

async function logoutUser() {
    try {
        let res = await agent
            .get('/logout')
            .send();

        res.should.have.status(204);
    }
    catch (err) {
        throw err;
    }
}

describe('Mongo API Tests', function () {
    this.timeout(0);

    const data = {
        name: 'Joseph Ho Zi Hao'
    };

    const datas = [
        { name: 'Joseph Ho Zi Hao' },
        { name: 'Scott Ryan Chew' },
        { name: 'Fang Sin' }
    ];

    /* Ensures Server has started before starting any tests */
    this.beforeAll((done) => {
        server.on('Server Started', () => {
            User.userSchema.deleteMany({}, (err) => {
                if (err)
                    throw err;

                let salt = bcrypt.genSaltSync(10);
                let newpassword = bcrypt.hashSync(AdminUser.password, salt);

                addUser(true, _.extend({}, AdminUser, { password: newpassword }))
                    .then(() => {
                        let salt = bcrypt.genSaltSync(10);
                        let newpassword = bcrypt.hashSync(notAdminUser.password, salt);

                        addUser(false, _.extend({}, notAdminUser, { password: newpassword }))
                            .then(() => {
                                done();
                            })
                            .catch((err) => {
                                console.error('\nError Adding notAdminUser', err); process.exit(1);
                            });
                    })
                    .catch((err) => {
                        console.error('\nError Adding AdminUser', err);
                        process.exit(1);
                    });
            });
        });
    });

    /* Before Each Test We Empty The Database */
    beforeEach((done) => {
        Schema["Test"].model.deleteMany({}, (err) => {
            if (err)
                throw err;
            done();
        });
    });

    afterEach((done) => {
        logoutUser()
            .then(() => {
                done();
            })
            .catch((err) => {
                throw err;
            });
    });

    after(() => {
        agent.close();
        serverobject.server.close();
        mongoose.connection.close();
    });

    describe('/POST test to route /api/insertRecord/:collection', function () {

        it('Should POST to `Test` Collection', (done) => {

            /* https://stackoverflow.com/questions/39662286/mocha-chai-request-and-express-session */
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .post('/api/insertRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send(data)
                        .then((res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('name');
                            done();
                        });
                });
        });

        it('Should return a 400 error on non-existent Path variable provided', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .post('/api/insertRecord/NullExist')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send(data)
                        .then((res) => {
                            res.should.have.status(400);
                            done();
                        });
                });
        });

        it('Should return a 400 error message on attempted POST of object with wrong property to `Test` Collection', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .post('/api/insertRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send({ property: 'Jonathan' })
                        .then((res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            assert.strictEqual('Request is not in the appropriate Schema Format', res.body.message);
                            done();
                        });
                });
        });

        it('Should return a 400 error message on attempted POST of object with existing entry in `Test` Collection', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    Schema["Test"].model.collection.insertOne(data);
                    agent
                        .post('/api/insertRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send(data)
                        .then((res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            done();
                        });
                });
        });
    });

    describe('/POST test to route /api/insertMultipleRecords/:collection', () => {
        it('Should POST to `TEST` collection and return 200 status message and array of Object', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .post('/api/insertMultipleRecords/Test')
                        .set({ 'content-type': 'application/json' })
                        .send(datas)
                        .then((res) => {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a('array');
                            res.body.should.have.length(datas.length);
                            res.body.map(entry => {
                                entry.should.have.property('name');
                            });
                            done();
                        });
                });
        });

        it('Should return a 400 error message on attempted POST of objects with any object containing a wrong property to `Test` Collection', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .post('/api/insertMultipleRecords/Test')
                        .set({ 'content-type': 'application/json' })
                        .send([{ name: 'Lester' }, { property: 'Jonathan' }])
                        .then((res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            assert.strictEqual('Request sent contains data not in the appropriate Schema Format', res.body.message);
                            done();
                        });
                });
        });

        it('Should return a 400 error on attempted POST of any objects already existing within the database', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    Schema["Test"].model.collection.insertMany(datas);
                    agent
                        .post('/api/insertMultipleRecords/Test')
                        .set({ 'content-type': 'application/json' })
                        .send(datas)
                        .then((res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            done();
                        });
                });
        });
    });

    describe('/DELETE test to route /api/deleteAllRecords/:collection', () => {

        it('Should DELETE all records in database', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    Schema["Test"].model.collection.insertMany(datas);
                    agent
                        .delete('/api/deleteAllRecords/Test')
                        .send()
                        .then((res) => {
                            res.should.have.status(204);
                            Schema["Test"].model.find({}, (err, result) => {
                                if (err) { throw err; }
                                assert(Array.isArray(result));
                                assert.strictEqual(result.length, 0);
                                done();
                            });
                        });
                });
        });

        it('Should return 204 response if attempting to delete from empty database', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .delete('/api/deleteAllRecords/Test')
                        .send()
                        .then((res) => {
                            res.should.have.status(204);
                            done();
                        });
                });
        });
    });

    describe('/DELETE test to route /api/deleteOneRecord/:collection', () => {
        it('Should delete one record in database if record exist', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    Schema["Test"].model.collection.insertOne({ name: 'Jonathan' });
                    agent
                        .delete('/api/deleteOneRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send({ name: 'Jonathan' })
                        .then((res) => {
                            res.should.have.status(204);
                            done();
                        });
                });
        });

        it('Should return a 400 error message on attempted DELETE of object in `Test` Collection with a non-existent property of `Test`', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .delete('/api/deleteOneRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send({ property: 'Jonathan' })
                        .then((res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            assert.strictEqual('Request is not in the appropriate Schema Format', res.body.message);
                            done();
                        });
                });
        });
    });

    describe('/GET test to route /api/queryOneRecord/:collection', () => {
        it('Should manage to query and return existing entries in database', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    Schema["Test"].model.collection.insertOne(data);
                    agent
                        .get('/api/queryOneRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send({ name: 'Joseph Ho Zi Hao' })
                        .then((res) => {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a('object');
                            res.body.should.have.property('name');
                            done();
                        });
                });
        });

        it('Should return a 400 error message on attempted GET of object in `Test` Collection with a non-existent property of `Test`', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .get('/api/queryOneRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send({ property: 'Jonathan' })
                        .then((res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            assert.strictEqual('Request is not in the appropriate Schema Format', res.body.message);
                            done();
                        });
                });
        });

        it('Should return 200 status message with result \'No Records were found.\' on attempt to GET an entry that doesn`t exist in database', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .get('/api/queryOneRecord/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send({ name: 'Jonathan' })
                        .then((res) => {
                            res.should.have.status(200);
                            res.should.be.json;
                            assert.deepEqual(res.body, 'No Records were found.');
                            done();
                        });
                });
        });
    });

    describe('/GET test to route /api/queryAllRecords/:collection', () => {
        it('Should manage to query and return all existing entries in database', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    Schema["Test"].model.collection.insertMany(datas);
                    agent
                        .get('/api/queryAllRecords/Test')
                        .set({ 'content-type': 'application/x-www-form-urlencoded' })
                        .send(data)
                        .then((res) => {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a('array');
                            assert.deepEqual(res.body.length, 3);
                            res.body.map(entry => {
                                entry.should.have.property('name');
                            });
                            done();
                        });
                });
        });
    });

    describe('/PUT test to route /api/updateOneRecord/:collection', () => {
        it('Should update property of existing objects', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {

                    Schema["Test"].model.collection.insertMany(datas);
                    let send = {
                        query: { name: 'Joseph Ho Zi Hao' },
                        update: {
                            name: 'ScottyBoy',
                            prop: 'anotherproperty'
                        }
                    };

                    agent
                        .put('/api/updateOneRecord/Test')
                        .set({ 'content-type': 'application/json' })
                        .send(send)
                        .then((res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('n');
                            res.body.should.have.property('nModified');
                            res.body.should.have.property('ok');
                            assert.deepEqual(res.body.nModified, 1);
                            done();
                        });
                });
        });

        it('Should return 400 error if request contains non-existent property of `Test` collection in query property', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .put('/api/updateOneRecord/Test')
                        .set({ 'content-type': 'application/json' })
                        .send({ query: { property: 'Jonathan' }, update: { name: 'Scotty' } })
                        .then((res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            assert.deepEqual(res.body.message, 'Request query is not in the appropriate Schema Format');
                            done();
                        });
                });
        });

        it('Should return 200 message with nModified == 0 on attempt to update an entry that does not exist in database', (done) => {
            loginUser(_.extend({}, AdminUser))
                .then(() => {
                    agent
                        .put('/api/updateOneRecord/Test')
                        .set({ 'content-type': 'application/json' })
                        .send({ query: { name: 'Hello' }, update: { name: 'Scotty' } })
                        .then((res) => {
                            res.should.have.status(200);
                            res.body.should.have.property('nModified');
                            assert.deepEqual(res.body.nModified, 0);
                            done();
                        });
                });
        });
    });
});