'use strict';

const assert = require('assert');

let chai = require('chai'),
    chaiHttp = require('chai-http'),
    serverobject = require('../../server'),
    server = serverobject.app,
    should = chai.should(),
    _ = require('underscore');

let mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

let User = require('../../app/models/MongoUsers'),
bcrypt = require('bcrypt-nodejs');

chai.use(chaiHttp);

const AdminUser = {
    username:'Jonathan',
    password:'password',
};

async function logoutUser(){ 
    try {
        let res = await chai.request(server)
                .get('/logout')
                .send();

        res.should.have.status(204);
    }
    catch(err) {
        throw err;
    }
}

describe('Test User Login Authentication', function () {
    this.timeout(0);

    /* Ensures Server has started before starting any tests */
    this.beforeAll((done)=>{
        server.on('Server Started', ()=>{
            done();
        });
    });

    /* Before Each Test We Empty The Database */
    beforeEach((done) => {
        User.userSchema.deleteMany({}, (err)=>{
            if(err){ throw err; }
            done();
        });
    });

    afterEach(() => {
        logoutUser();
    });

    after(()=>{
        User.userSchema.deleteMany({}, (err)=>{
            if(err){ throw err; }
        });
        serverobject.server.close();
        mongoose.connection.close(); 
    });

    describe('/POST Test to route /signup', ()=>{
        it('Should sign up the user and return an object indicating successful signup', (done)=>{
            chai.request(server)
                    .post('/signup')
                    .set({'content-type' : 'application/json'})
                    .send(_.extend({}, AdminUser))
                    .end((err,res)=>{
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message');
                        res.body.should.have.property('user');
                        res.body.user.should.have.property('username');
                        assert.deepEqual(res.body.user.username, AdminUser.username)
                        assert.deepEqual(res.body.message, 'Signup Was Successful');
                        done();
                    });
        });

        it('Should return a 400 status message if attempt to add a user that already exists', (done)=>{
            let salt = bcrypt.genSaltSync(10);
            let encryptpass = bcrypt.hashSync(AdminUser.password, salt);
            let adduser = _.extend({}, AdminUser, { password: encryptpass });
            User.userSchema.collection.insertOne(adduser, ()=>{
                chai.request(server)
                    .post('/signup')
                    .set({'content-type' : 'application/json'})
                    .send(_.extend({}, adduser))
                    .end((err,res)=>{
                        res.should.have.status(400);
                        done();
                    });
            });
        });
    });

    describe('/POST Test to route /login', ()=>{
        it('Should return a 200 status message on successful login', (done)=>{
            let salt = bcrypt.genSaltSync(10);
            let encryptpass = bcrypt.hashSync(AdminUser.password, salt);
            let adduser = _.extend({}, AdminUser, { password: encryptpass });
            User.userSchema.collection.insertOne(adduser, ()=>{
                chai.request(server)
                    .post('/login')
                    .set({'content-type' : 'application/json'})
                    .send(_.extend({}, AdminUser))
                    .end((err,res)=>{
                        if(err){ throw err; }
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.should.have.property('user');
                        res.body.user.should.have.property('_id');
                        res.body.user.should.have.property('username');
                        assert.deepEqual(res.body.message, 'Login is Successful');
                        assert.deepEqual(res.body.user.username, AdminUser.username);
                        done();
                    });
            });
        });

        it('Should return a 400 status message if provided username does not exist', (done)=>{
            chai.request(server)
                    .post('/login')
                    .set({'content-type' : 'application/json'})
                    .send({username:'John', password:'password'})
                    .end((err,res)=>{
                        res.should.have.status(400);
                        assert.deepEqual(res.body, 'Invalid User Credentials or Error Logging In.');
                        done();
                    });
        });
    });

});