'use strict';

const gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    eslint = require('gulp-eslint'),
    mocha = require('gulp-mocha'),
    glob = require('glob'),
    run = require('gulp-run-command').default;

/** 
 * Tasks Section Necessary For Setting Environment> 
 */
gulp.task('set_env_to_test', run('cross-env NODE_ENV=test'));

function MochaPipe(filepath) {
    this.filepath = filepath;

    this.returnPipe = function () {
        return gulp.src(filepath)
            .pipe(mocha({
                reporter: 'list'
            }));
    }
}

function createGulpTask(name, callback) {
    gulp.task(name, callback);
}

function getArrayOfFilePathFromGlobPattern(globpattern) {
    return glob.sync(globpattern);
}

function mochaCreateTasks(filenames) {
    try {
        let tasks = [];

        for (let i in filenames) {
            let taskName = 'Test ' + filenames[i];
            tasks.push(taskName);
            createGulpTask(taskName, new MochaPipe(filenames[i]).returnPipe);
        }

        return tasks;
    }
    catch (error) {
        console.error(error);
        process.exit(1);
    }
}

function mochaTestMongo() {
    let filenames = ['./tests/mocha/mochapassporttest.js']
        .concat(getArrayOfFilePathFromGlobPattern('./tests/mocha/mongo/*.js'));

    let tasks = mochaCreateTasks(filenames);
    let perform = ['set_env_to_test', 'eslint'].concat(tasks);
    return gulp.series(perform);
}

function mochaTestPostGreSql() {
    let filenames = ['./tests/mocha/mochapassporttest.js']
        .concat(getArrayOfFilePathFromGlobPattern('./tests/mocha/mochapostgresqltest.js'));;

    let tasks = mochaCreateTasks(filenames);
    let perform = ['set_env_to_test', 'eslint'].concat(tasks);
    return gulp.series(perform);
}

gulp.task('eslint', () => {
    let options = {
        rules: {
            eqeqeq: ['error', 'always', { null: 'ignore' }],
            quotes: ['error', 'single']
        },
        parserOptions: { ecmaVersion: 2017 },
        env: { es6: true }
    };

    return gulp.src(['./app/**/*.js'])
        .pipe(eslint(options))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('nodemon_task', function () {
    nodemon({
        script: './server.js',
        tasks: ['eslint'],
        env: { 'NODE_ENV': 'development' }
    });
});

gulp.task('forever', run('cross-env NODE_ENV=production forever start -m 3 --minUptime 1000 --spinSleepTime 1000 -c node server.js'))

/** 
 * gulp.series() means tasks will be executed sequentially
 */

gulp.task('default', gulp.series('eslint', 'nodemon_task'));
gulp.task('deploy', gulp.series('eslint', 'forever'));

gulp.task('testmongo', mochaTestMongo());
gulp.task('testpostgresql', mochaTestPostGreSql());