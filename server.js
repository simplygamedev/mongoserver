'use strict';

const https = require('https'),
    express = require('express'),
    session = require('express-session'),
    passport = require('passport'),
    fs = require('fs');

const env = process.env.NODE_ENV !== undefined 
    ? process.env.NODE_ENV 
    : 'test';
    
const config = require(`./config/env/${env}`);

const app = express();
const session_handler = session({
    secret: config.express.session_secret,
    saveUninitialized: true,
    resave: true,
    cookie: { secure: false }
});

app.use(session_handler);
app.use(passport.initialize());
app.use(passport.session());

const _export = {
    app: app
};

const mongoose_config = {
    useNewUrlParser: true
};

const options = {
    key: fs.readFileSync('private.pem'),
    cert: fs.readFileSync('public.pem')
};

const start_server = () => {
    _export.server = https.createServer(options, app)
    _export.server.listen(config.port);

    require('./app/routes/main_route')(app);
    require('./app/routes/database_routes')(app);
    require('./app/routes/users_route')(app);

    console.log(`\nListening on port ${config.port}\n`);

    /* Mocha Test Runner is using the .emit() method below to know 
       when the server has started before beginning tests */
    app.emit('Server Started');
};

switch (config.db) {
    case 'mongodb':
        const mongoose = require('mongoose');
        mongoose.set('useCreateIndex', true);
        mongoose
            .connect(config.mongodb.url, mongoose_config)
            .then(start_server);
        break;
    default:
        start_server();
}

/* Expose app for Testing Below */
module.exports = _export;