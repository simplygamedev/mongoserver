'use strict';

module.exports = {
    port: 443,
    db: 'mongodb',
    url: 'https://localhost:443',
    mongodb: {
        url: 'mongodb://localhost:27017/mongoDB-prod',
    },
    express: {
        session_secret: 'prod_secret'
    },
    passportjs: {
        authentication: 'passport-session'
    }
}