'use strict';

module.exports = {
    port: 3000,
    //db:'postgresql',
    db: 'mongodb',
    url: 'http://localhost:3000',
    mongodb: {
        //MongoDB will create the database if it does not exist, and make a connection to it.
        url: 'mongodb://localhost:27017/mongoDB-dev',
    },
    express: {
        session_secret: 'dev_secret'
    },
    passportjs: {
        authentication: 'passport-session'
    },
    postgresql: {
        user: 'postgres',
        host: 'localhost',
        database: 'Development',
        password: 'Qwerty1995!',
        port: 5432,
    }
}