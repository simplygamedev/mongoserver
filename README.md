# MongoServer

## Getting Started
---
1. Install mochaJS (E.g. npm install -g mocha)
2. Install gulp-cli (E.g. npm install --global gulp-cli)
3. Install gulp-nodemon (E.g. npm install --save-dev gulp-nodemon)
4. Run npm install

## _Creating a new Model Schema for MongoDB_
- There are two templates files that you can refer to so that you can create your own MongoDB Model Schema.
- This are contained under the folder '/app/models/MongoDBSchemas'.
- The two files are 'testSchema' & 'schemaTemplate'.
- Once you have your own Schema file created, go to '/app/models/Schemas.js' & add in a Reference to your Model's Schema.
- A new collection will be created in your MongoDB database once you start up the server.

## _Mocha Test Template Generation for Mongo CRUD Using Yeoman_
- This allows us to generate new tests for testing new Mongo Models that we intend to create.
- Make sure you have the yeoman utility installed using 'npm install -g yo'.
- From the command line, change to the folder 'generator-mocha-test' and run 'yo mocha-test <Name of your Model to Create Test For >'.
- This will generate a template mocha test for your Model under the folder '/tests/mocha/mongo' 

## _Note_
- The server can be started to receive http requests by typing 'gulp' in the command line in the project's directory.
- The command 'gulp testmongo' executes the mochaJS MongoDB test for this project.
- The command 'gulp testpostgresql' executes the mochaJS PostGreSql test for this project. The tests for PostGreSql currently do not test anything.
- Do make sure to replace the .pem files with your own, as the one's provided are for testing purposes only

## _Current Project status_
- Future Plans to add OAuth Server & Client for Authentications
- At Present, PostGreSql implementation only works for the insertRecord route.
- There are presently no plans to embellish details for the other routes as it is a POC implementation.