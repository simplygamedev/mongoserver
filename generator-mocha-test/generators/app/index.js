'use strict';

const Generator = require('yeoman-generator');

module.exports = class extends Generator {

    constructor(args, opts) {
        // Calling the super constructor is important so our generator is correctly set up
        super(args, opts);
    
        this.argument("model", { type: String, required: true });

        // Next, add your custom code
        this.option('babel'); // This method adds support for a `--babel` flag
    }

    writing() {
        this.fs.copyTpl(
          this.templatePath('mocha-generator.js'),
          this.destinationPath(`../tests/mocha/mongo/mochamongo${this.options['model'].toLowerCase()}test.js`),
          { model: this.options['model'] }
        );
    }
};